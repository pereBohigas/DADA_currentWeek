package edu.uoc.android.currentweek;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class IntentTest {

    private static Integer currentWeekValue;
    private static Calendar calendar;

    @Rule
    public IntentsTestRule<MainActivity> IntentsTestRule = new IntentsTestRule<>(MainActivity.class);

    @Before
    public void setUp(){
        calendar = Calendar.getInstance();
        currentWeekValue = calendar.get(Calendar.WEEK_OF_YEAR);
    }

    @Test
    public void checkIntentDataRight() {
        // Type value into the input field
        onView(withId(R.id.currentWeekInput))
                .perform(typeText(String.valueOf(currentWeekValue)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.buttonCheck))
                .perform(click());

        // Check the intent'data
        intended(allOf(
                hasExtra(MainActivity.EXTRA_CURRENTWEEKVALUE, currentWeekValue)));
    }

    @Test
    public void checkIntentDataWrong() {
        // Type value into the input field
        onView(withId(R.id.currentWeekInput)).perform(typeText(String.valueOf(currentWeekValue-1)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.buttonCheck)).perform(click());

        // Check the intent's data
        intended(allOf(
                hasExtra(MainActivity.EXTRA_CURRENTWEEKVALUE, (currentWeekValue-1))));
    }

}
