package edu.uoc.android.currentweek.utilities;

import java.util.Calendar;

public class CalendarUtility {

    private final Calendar calendar;

    public CalendarUtility() {
        this.calendar = Calendar.getInstance();
    }

    /** Method to get the current week number **/
    private int getCurrentWeekNumber() {
        return this.calendar.get(Calendar.WEEK_OF_YEAR);
    }

    /** Method to check the current week number **/
    public boolean checkCurrentWeekNumber(int weekNumberValue) {
        return getCurrentWeekNumber() == weekNumberValue;
    }

}
