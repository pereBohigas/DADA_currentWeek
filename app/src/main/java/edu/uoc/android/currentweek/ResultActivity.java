package edu.uoc.android.currentweek;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.uoc.android.currentweek.utilities.CalendarUtility;

public class ResultActivity extends Activity implements View.OnClickListener {

    private Button buttonAgain;
    private TextView textResult;

    private Integer weekNumberValue;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        this.setViews();

        /** Get the intent that started this activity and extract the entered week number **/
        Intent intent = getIntent();
        weekNumberValue = intent.getIntExtra(MainActivity.EXTRA_CURRENTWEEKVALUE, 0);

        this.checkWeekNumberValue();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /** Check if there is file associated with the media player and it's still playing **/
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            /** Stop it **/
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    /** Method to set views from activity_result.xml to this class **/
    private void setViews() {
        /** Link result's text view with local variable **/
        textResult = findViewById(R.id.textResult);
        /** Link button with local variable **/
        buttonAgain = findViewById(R.id.buttonAgain);
        /** Implement OnClickListener interface to wait for a click event **/
        buttonAgain.setOnClickListener(this);
    }

    /** Method to check the entered week number with the CalendarUtility class **/
    private void checkWeekNumberValue() {
        CalendarUtility calendarUtility = new CalendarUtility();
        boolean isCorrectValue = calendarUtility.checkCurrentWeekNumber(weekNumberValue);

        this.setTextDependingOnResult(isCorrectValue);
        this.setButtonDependingOnResult(isCorrectValue);
        this.playAudioDependingOnResult(isCorrectValue);
    }

    private void setTextDependingOnResult(boolean result) {
        if (result) {
            /** Set text and color for a right answer **/
            textResult.setText(R.string.results_right);
            textResult.setTextColor(Color.GREEN);
        } else {
            /** Set text for a wrong answer (it uses the default color) **/
            textResult.setText(R.string.results_wrong);
        }
    }

    private void setButtonDependingOnResult(boolean result) {
        if (result) {
            /** Set button's text for a right answer (for wrong uses the default one) **/
            buttonAgain.setText(R.string.results_buttonRight);
        }
    }

    private void playAudioDependingOnResult(boolean result) {
        if (result) {
            /** Set the right answer's sound **/
            mediaPlayer = MediaPlayer.create(this, R.raw.correct_answer);
        } else {
            /** Set the wrong answer's sound **/
            mediaPlayer = MediaPlayer.create(this, R.raw.wrong_answer);
        }
        /** Play the selected sound **/
        mediaPlayer.start();
    }

    /** Method called to go to MainActivity **/
    private void backToMainActivity() {
        /** Create an intent to go to MainActivity **/
        Intent intent = new Intent(this, MainActivity.class);
        /** Start the MainActivity **/
        startActivity(intent);
    }

    @Override
    /** Method called when the user touches de button **/
    public void onClick(View view) {
        this.backToMainActivity();
    }

}
